# README #

This Repo is intended to serve as the central point of projects or coding challenges to experiment/learn new languages or technologies.

### What is this repository for? ###

* This Repo is intended to serve as the central point of projects or coding challenges to experiment/learn new languages or technologies.
* Add new project ideas or skeletons to the project here.  




### How do I get set up? ###

* Naviagate to directory of the project or challenge you are interested in working on.
* Follow any instructions and download any necessary files included in the directory of the project.



### Contribution guidelines ###

* Have an idea for a project? Clone the directory and put in a pull request with the addition of any necessary files for it under a new directory and attach instructions as a markdown (.md)



### Who do I talk to? ###

* Current repo owner robert.hubert@infinitive.com
* Other community or team contact