Here is a ZIP file containing CSVs with the results of the 2016 Iowa Caucuses:

 

Using that data, write a logic to produce a JSON API with the following endpoints:




GET Request with endpoint http://your-service-here/counties 
Returns all counties with election results in the following format:

 
    {

    "counties": [

      {

        "name": "Adair",

        "fips": "19001",

        "elections": [

          {

            "party": "Democratic",

            "results": [

              {"candidate": "Hillary Clinton", "votes": 113},

              {"candidate": "Bernie Sanders", "votes": 86},

              {"candidate": "Martin O'Malley","votes": 0}

            ]

          }

          {

            "party": "Republican",

            "results": [

              {"candidate": "Ted Cruz","votes": 104},

              {"candidate": "Donald Trump","votes": 104}

              ...

            ]

          }

        ]

      },

      {

        "name": "Adams",

        "fips": "19003",

        "elections": [

          ...

        ]

      }

    ]

    }





2.      GET Request with endpoint http://your-service-here/counties?democratic_winner=Hillary%20Clinton&republican_winner=Ted%20Cruz

      Returns the above results, but filtered to only show counties where the specified candidate received the most votes (including ties).





3.      GET Request with endpoint http://your-service-here/counties/19001

Returns the results for the county with the specified FIPS code.

 

    {

    "name": "Adair",

    "fips": "19001",

    "elections": [

      {

        "party": "Democratic",

        "results": [

          {"candidate": "Hillary Clinton", "votes": 113},

          {"candidate": "Bernie Sanders","votes": 86},

          {"candidate": "Martin O'Malley","votes": 0}

        ]

      }

      {

        "party": "Republican",

        "results": [

          {"candidate": "Ted Cruz","votes": 104},

          {"candidate": "Donald Trump","votes": 104}

          ...

        ]

      }

    ]

    }

 

Take a look at framework like Flask and Flask-Restful, you can quickly spin up an API in python with those two. There is also an even lighter framework called “Bottle” that you can use to build simple API endpoint.

Try to tackle the 1st and 3rd endpoints because trying the second one, that’s a little more involved.

Let me know if you have any question.

 